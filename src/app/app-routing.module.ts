import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MegaMillionsComponent } from './mega-millions/mega-millions.component';
import{HomeComponent } from './home/home.component'


const routes: Routes = [{path:'', redirectTo:'Home', pathMatch: 'full'},
 {path:'Home',component:HomeComponent},
 {path:'MegaMillions',component:MegaMillionsComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
