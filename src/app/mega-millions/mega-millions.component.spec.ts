import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MegaMillionsComponent } from './mega-millions.component';

describe('MegaMillionsComponent', () => {
  let component: MegaMillionsComponent;
  let fixture: ComponentFixture<MegaMillionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MegaMillionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MegaMillionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
