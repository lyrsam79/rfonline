import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-mega-millions',
  templateUrl: './mega-millions.component.html',
  styleUrls: ['./mega-millions.component.css']
})
export class MegaMillionsComponent implements OnInit {
  today: number = Date.now();
   Edited:boolean;
  constructor() { }

  ngOnInit(): void {
    this.Edited=true;
  }

  public ConfirmGame():Observable<void>{
  
     this.Edited=false;
     return;

  }
}
